#include <iostream>

using namespace std;

int fact(int);


int main()
{
  int current;
  
  cout<<"Enter a positive integer [to terminate enter non-positive] > ";
  cin >>current;
  
  while (current > 0)
  {
    cout <<"The factorial of "<<current<<" is "<< fact(current)<<"\n";
    cout<<"\nEnter a positive integer [to terminate enter non-positive] > ";
    cin>>current;
  
  }
 
}


int fact(int n)
{
  int lcv, p;
  
  for(p = 1, lcv = 2; lcv <= n; p*=lcv, lcv++);
  
  return p;
}